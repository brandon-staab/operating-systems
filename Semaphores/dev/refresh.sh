#!/bin/sh

rm -f -r ../build &&
mkdir ../build && (
	cd ../build &&
	cmake .. -DCMAKE_BUILD_TYPE=DEBUG -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
) &&
reset &&
echo "Refreshed"
