#!/bin/sh

reset && (
	cd ../build &&

	make --jobs=1 &&
	make --jobs=1 test &&

	echo "Running..." &&
	./src/smokers &&

	echo "Complete"
)
