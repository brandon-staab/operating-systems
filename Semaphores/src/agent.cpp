#include "agent.hpp"

#include "GLOBALS.hpp"
#include "random_delay.hpp"


void*
agent_tobacco_paper(void*) {
	auto supplied = 0;

	while (supplied++ < 6) {
		// wait to be told what to create
		sem_wait(&agent_sem);

		// create tobacco and paper
		random_delay(50, 200);

		// supply the pushers
		sem_post(&table_top.tobacco.ingredient);
		sem_post(&table_top.paper.ingredient);
	}

	pthread_exit(0);
}


void*
agent_tobacco_match(void*) {
	auto supplied = 0;

	while (supplied++ < 6) {
		// wait to be told what to create
		random_delay(50, 200);

		// create tobacco and match
		sem_wait(&agent_sem);

		// supply the pushers
		sem_post(&table_top.tobacco.ingredient);
		sem_post(&table_top.match.ingredient);
	}

	pthread_exit(0);
}


void*
agent_paper_match(void*) {
	auto supplied = 0;

	while (supplied++ < 6) {
		// wait to be told what to create
		sem_wait(&agent_sem);

		// create paper and match
		random_delay(50, 200);

		// supply the pushers
		sem_post(&table_top.paper.ingredient);
		sem_post(&table_top.match.ingredient);
	}

	pthread_exit(0);
}
