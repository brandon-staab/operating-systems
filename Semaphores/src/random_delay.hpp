#pragma once

#include <cstddef>


void random_delay(std::size_t const min_delay, std::size_t const max_delay) noexcept;
