#include "smoker.hpp"

#include "GLOBALS.hpp"
#include "random_delay.hpp"

#include <cstdio>


void*
smoker_tobacco(void*) {
	auto smoked = 0;

	while (smoked++ < 3) {
		sem_wait(&table_top.tobacco.sem);

		// make the cigarette
		printf("The tobacco guy is rolling a cigarette...\n");
		random_delay(20, 50);

		// let the agent do his job
		sem_post(&agent_sem);

		// smoke the cigarette
		printf("Smoking the tobacco guy's %ith cigarette...\t\tT%i\n", smoked, smoked);
		random_delay(20, 50);
	}

	pthread_exit(0);
}


void*
smoker_paper(void*) {
	auto smoked = 0;

	while (smoked++ < 3) {
		sem_wait(&table_top.paper.sem);

		// make the cigarette
		printf("The paper guy is rolling a cigarette...\n");
		random_delay(20, 50);

		// let the agent do his job
		sem_post(&agent_sem);

		// smoke the cigarette
		printf("Smoking the paper guy's %ith cigarette...\t\tP%i\n", smoked, smoked);
		random_delay(20, 50);
	}

	pthread_exit(0);
}

void*
smoker_match(void*) {
	auto smoked = 0;

	while (smoked++ < 3) {
		sem_wait(&table_top.match.sem);

		// make the cigarette
		printf("The match guy is rolling a cigarette...\n");
		random_delay(20, 50);

		// let the agent do his job
		sem_post(&agent_sem);

		// smoke the cigarette
		printf("Smoking the match guy's %ith cigarette...\t\tM%i\n", smoked, smoked);
		random_delay(20, 50);
	}

	pthread_exit(0);
}
