#include "get_random_int.hpp"

#include <random>


int
get_random_int(int const low, int const high) noexcept {
	static auto rd = std::random_device{};

	auto generator = std::default_random_engine{rd()};
	auto distribution = std::uniform_int_distribution<int>{low, high};

	return distribution(generator);
}
