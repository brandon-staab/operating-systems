#include "GLOBALS.hpp"
#include "Ingredients.hpp"
#include "agent.hpp"
#include "pusher.hpp"
#include "random_delay.hpp"
#include "smoker.hpp"

#include <array>
#include <cstdio>


int
main() {
	auto constexpr pshared = 0;
	table_top = Ingredients();

	sem_init(&agent_sem, pshared, 1);
	sem_init(&mutex, pshared, 1);

	std::array<pthread_t, 3> agents;
	std::array<pthread_t, 3> pushers;
	std::array<pthread_t, 6> smokers;

	// create smokers
	pthread_create(&smokers[0], nullptr, smoker_tobacco, nullptr);
	pthread_create(&smokers[1], nullptr, smoker_tobacco, nullptr);
	pthread_create(&smokers[2], nullptr, smoker_paper, nullptr);
	pthread_create(&smokers[3], nullptr, smoker_paper, nullptr);
	pthread_create(&smokers[4], nullptr, smoker_match, nullptr);
	pthread_create(&smokers[5], nullptr, smoker_match, nullptr);

	// create agents
	pthread_create(&agents[0], nullptr, agent_tobacco_paper, nullptr);
	pthread_create(&agents[1], nullptr, agent_tobacco_match, nullptr);
	pthread_create(&agents[2], nullptr, agent_paper_match, nullptr);

	// create pushers
	pthread_create(&pushers[0], nullptr, pusher_tobacco, nullptr);
	pthread_create(&pushers[1], nullptr, pusher_paper, nullptr);
	pthread_create(&pushers[2], nullptr, pusher_match, nullptr);

	// join threads
	for (auto thread : agents) pthread_join(thread, nullptr);
	for (auto thread : pushers) pthread_join(thread, nullptr);
	for (auto thread : smokers) pthread_join(thread, nullptr);

	printf("Everyone is smoked out and hungry...\n");
}
