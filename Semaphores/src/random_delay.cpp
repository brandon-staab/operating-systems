#include "random_delay.hpp"

#include "get_random_int.hpp"

#include <chrono>
#include <thread>


void random_delay(std::size_t const min_delay, std::size_t const max_delay) noexcept {
	auto const delay = get_random_int(min_delay, max_delay);

	std::this_thread::sleep_for(std::chrono::milliseconds(delay));
}
