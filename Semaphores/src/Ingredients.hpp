#pragma once

#include <pthread.h>
#include <semaphore.h>


struct Ingredient {
	Ingredient();

	bool on_table;
	sem_t ingredient;
	sem_t sem;
};

struct Ingredients {
	Ingredient tobacco;
	Ingredient paper;
	Ingredient match;
};
