#include "pusher.hpp"

#include "GLOBALS.hpp"


void*
pusher_tobacco(void*) {
	auto pushed = 0;

	while (pushed++ < 12) {
		sem_wait(&table_top.tobacco.ingredient);

		sem_wait(&mutex);
		if (table_top.paper.on_table) {
			table_top.paper.on_table = false;
			sem_post(&table_top.match.sem);
		} else if (table_top.match.on_table) {
			table_top.match.on_table = false;
			sem_post(&table_top.paper.sem);
		} else {
			table_top.tobacco.on_table = true;
		}
		sem_post(&mutex);
	}

	pthread_exit(0);
}


void*
pusher_paper(void*) {
	auto pushed = 0;

	while (pushed++ < 12) {
		sem_wait(&table_top.paper.ingredient);

		sem_wait(&mutex);
		if (table_top.tobacco.on_table) {
			table_top.tobacco.on_table = false;
			sem_post(&table_top.match.sem);
		} else if (table_top.match.on_table) {
			table_top.match.on_table = false;
			sem_post(&table_top.tobacco.sem);
		} else {
			table_top.paper.on_table = true;
		}
		sem_post(&mutex);
	}

	pthread_exit(0);
}


void*
pusher_match(void*) {
	auto pushed = 0;

	while (pushed++ < 12) {
		sem_wait(&table_top.match.ingredient);

		sem_wait(&mutex);
		if (table_top.tobacco.on_table) {
			table_top.tobacco.on_table = false;
			sem_post(&table_top.paper.sem);
		} else if (table_top.paper.on_table) {
			table_top.paper.on_table = false;
			sem_post(&table_top.tobacco.sem);
		} else {
			table_top.match.on_table = true;
		}
		sem_post(&mutex);
	}

	pthread_exit(0);
}
