#include "Ingredients.hpp"


Ingredient::Ingredient() : on_table(false) {
	auto constexpr pshared = 0;
	auto constexpr value = 0;

	sem_init(&ingredient, pshared, value);
	sem_init(&sem, pshared, value);
}
