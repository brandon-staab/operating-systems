#pragma once

#include "Ingredients.hpp"

#include <semaphore.h>

#include <cstddef>


extern sem_t agent_sem;
extern sem_t mutex;
extern Ingredients table_top;
