Changelog
=========

Lab 1
-----
In lab one I created a compile script and kernel.  Next, I made a function that prints a `char` to the teletype output or the parallel port output.  Next, I made a print string function that called the `print_char` function.  Finally, I use interrupt `0x21` to make a system interrupt interface.  After implementing the kernel for lab one, I created the driver function as instructed.


Lab 2
-----
For lab two I implemented three core functions for the kernel.
1. `read_string`
2. `write_int`
3. `read_int`

In order to read strings from the console I wrote a function that uses an interrupt to grab keystrokes one by one and then fills a buffer.

In order to process `int`s I had to create a `divide` function.  After making divide I made two helper functions `div` and `mod`.  After this, I made a function that converts `int`s into strings.  I used this in combination with `print_string` to make the `write_int` function.  To read `int`s in, I first attempted to convert a string from the `read_string` function to an `int`.  After this, I converted the int back to a string if it may have been larger than a 16-bit `int`.  I then compare them to ensure they are the same.

After the core functions were implemented I added them to the system interrupt interface.  Upon completing this I modified my `compile_os.sh` script to support multiple driver functions.  I then separated `lab 1`'s `main` function into `main_lab01.c` and `lab 2`'s `main` function into `main_lab02.c`.  Upon completing this I modified lab 2's driver function to the mad lib program provided.


Lab 3
-----
For lab two I implemented three core functions for the kernel.
1. `read_sector`
2. `write_sector`
3. `clear_screen`

To create the `read_sector` and `write_sector` functions I made two functions to assist in converting the absolute sector number to a relative sector number of the format `Track:Head:Sector`.  This first function converts the absolute function to the `T:H:S` format.  The next function converts this information along with a device number so that the BIOS can use it.  Both `read_sector` and `write_sector` use this output to operate on a sector.

`clear_screen` is a pretty straight forward function.  Fist is prints `24` new lines to the teletype output.  Then it moves the cursor back to the top right of the screen.  Finally, if a valid color code pair is provided, it sets the background and foreground colors.

Finally, I refactored some error handling and made it so that the `read_string` function does not overflow the buffer if a user enters to much data.


Lab 4
-----
In lab four I made a file system.  To do this I made four core functions for the kernel.

1. `read_file`
2. `write_file`
3. `delete_file`
4. `error`

The `error` function takes care of all the BlackDOS error messages including errors related to the file system.  `read_file` takes a file off of the disk and puts it into memory.  It does this by consulting the directory and loading corresponding sectors.  `write_file` finds a space in the directory and stores and entry to the file.  Each entry is `32 bytes` long.  The first `8 bytes` are the file's name.  The remaining `24 bytes` are the sectors on the disk that make up the file.  The file is written to these sectors and marked as used in the file system's map.  When the `delete_file` function is called, the first byte of the entry is nulled out and the used sectors in the map of the file system is set to unused.  If any of these functions can not complete their task they return with an error.


Lab 5
-----
I implemented the following functions.

1. `run_program`
2. `stop`

`run_program` loads a program into memory and the launches it.  `stop` makes the computer hang using a busy loop.  I also had to modify existing kernel code to not use the text space to properly take advantage of the operating systems interrupt design regarding changing the base pointer register.

Lab 6
-----
I made a shell for the operating system.  The shell rocognizes files that start with a capital letter as system files.  The shell supports the following commands:

* boot                           - reboot the system
* clrs                           - clear the terminal screen
* copy <source> <destination>    - copy file
* ddir                           - list disk directory contents
* echo <comment>                 - display a line of text
* help                           - display the user manual
* move <source> <destination>    - move a file
* prnt <filename>                - prints file on printer
* remv <filename>                - remove file
* senv                           - set environment variables
* show <filename>                - prints file on terminal
* twet <filename>                - writes twet to file
