#!/bin/sh

######################
#    Lab Selector    #
LAB='06'             #
#                    #
# Example:           #
#   LAB='01'         #
######################

####################
#    Color Mode    #
####################
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'
