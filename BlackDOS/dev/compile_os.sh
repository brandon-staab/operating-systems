#!/bin/sh

. ./config.sh


ready_disk() {
	echo "${RED}Readying disk${NC}" &&

	echo "${YELLOW}INFO:${NC} create empty disk image" &&
	dd if=/dev/zero of=../build/floppya.img bs=512 count=2880 &&

	echo "${YELLOW}INFO:${NC} assemble bootloader" &&
	nasm ../src/bootload.asm -o ../build/bootload &&

	echo "${YELLOW}INFO:${NC} add bootloader to disk at sector 0" &&
	dd if=../build/bootload of=../build/floppya.img bs=512 count=1 conv=notrunc &&

	echo "${YELLOW}INFO:${NC} add map to disk at sector 256" &&
	dd if=../assets/map of=../build/floppya.img bs=512 count=1 seek=256 conv=notrunc &&

	echo "${YELLOW}INFO:${NC} add config to disk at sector 258" &&
	dd if=../assets/config of=../build/floppya.img bs=512 count=1 seek=258 conv=notrunc
}


compile_file() {
	echo "${YELLOW}INFO:${NC} compile ${GREEN}$1.c${NC}" &&
	bcc -ansi -c ../src/$1.c -o ../build/$1.o
}


assemble_file() {
	echo "${YELLOW}INFO:${NC} assemble ${GREEN}$1.asm${NC}" &&
	as86 ../src/$1.asm -o ../build/$2.o
}


compile_kernel() {
	echo "${RED}Compiling kernel${NC}" &&

	echo "${YELLOW}INFO:${NC} compile ${GREEN}main.c${NC} for ${GREEN}lab ${LAB}${NC}" &&
	bcc -ansi -c ../src/main_lab${LAB}.c -o ../build/main.o &&

	compile_file library &&
	compile_file kernel &&
	assemble_file syscall/interrupt interrupt &&
	assemble_file kernel kasm &&

	echo "${YELLOW}INFO:${NC} link ${GREEN}kernel${NC}" &&
	ld86                      \
	  -d ../build/main.o      \
	     ../build/kasm.o      \
	     ../build/interrupt.o \
	     ../build/kernel.o    \
	     ../build/library.o   \
	  -o ../build/kernel &&

	echo "${YELLOW}INFO:${NC} add kernel to disk at sector 259" &&
	dd if=../build/kernel of=../build/floppya.img bs=512 conv=notrunc seek=259
}


compile_program() {
	echo "${YELLOW}INFO:${NC} compile ${GREEN}$1${NC}" &&
	. ../assets/programs/$1/compile.sh
}


compile_programs() {
	echo "${RED}Compiling programs${NC}" &&
	compile_program Shell &&
	compile_program Help &&
	compile_program cal &&
	compile_program fib &&
	compile_program t3
}


load_file() {
	echo "${YELLOW}INFO:${NC} loading ${GREEN}$2${NC} from ${GREEN}$1${NC}" &&
	./load_file $1 $2
}


load_files() {
	echo "${RED}Loading files${NC}" &&

	echo "${YELLOW}INFO:${NC} compile ${GREEN}load_file.c${NC}" &&
	gcc ../tools/load_file.c -o ../build/load_file &&

	echo "${YELLOW}INFO:${NC} load files and update filesystem" && (
		cd ../build &&

		echo "${YELLOW}INFO:${NC} add files" &&
		load_file ../assets/Welcome Welcome &&
		load_file ../assets/spc03 spc03 &&

		echo "${YELLOW}INFO:${NC} add programs" &&
		load_file ../build/Shell Shell &&
		load_file ../build/Help Help &&
		load_file ../assets/programs/Stenv Stenv &&
		load_file ../assets/programs/kitty1 kitty1 &&
		load_file ../assets/programs/kitty2 kitty2 &&
		load_file ../build/cal cal &&
		load_file ../build/fib fib &&
		load_file ../build/t3 t3
	)
}


clear -x &&
ready_disk &&
compile_kernel &&
compile_programs &&
load_files &&
echo "${YELLOW}INFO:${GREEN} compilation successful${NC}"
