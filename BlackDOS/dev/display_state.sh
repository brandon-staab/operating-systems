#!/bin/sh

. ./config.sh


clear -x && (
	cd ../run &&

	echo "${GREEN}Map${NC}" &&
	hexdump -v -C -s 0x20000 -n 256 floppya.img &&

	echo "${GREEN}Directory${NC}" &&
	hexdump -v -C -s 0x20200 -n 512 floppya.img &&

	echo "${GREEN}Config:${NC}" &&
	hexdump -C -s 0x20400 -n 512 floppya.img
)
