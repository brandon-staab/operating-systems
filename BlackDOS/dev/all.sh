#!/bin/sh

. ./config.sh


reset && (
	echo "${YELLOW}INFO:${GREEN} cleaning${NC}" &&
	rm -f ../run/bochs.out &&
	rm -f ../run/floppya.img &&
	rm -f ../run/printer.out &&
	rm -f -r ../build/ &&
	mkdir ../build/ &&

	echo "${YELLOW}INFO:${GREEN} running compile_os.sh${NC}" &&
	./compile_os.sh &&

	echo "${YELLOW}INFO:${GREEN} copying BlackDOS to run directory${NC}" &&
	cp ../build/floppya.img ../run/floppya.img &&

	echo "${YELLOW}INFO:${GREEN} running run.sh${NC}" &&
	./run.sh
)
