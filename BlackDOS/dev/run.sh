#!/bin/sh

. ./config.sh


clear -x && (
	cd ../run &&

	if [ -f "./floppya.img" ]
	then
		echo "${RED}Running BlackDOS on bochs${NC}" &&
		echo "c" | bochs -f bdos.txt
		clear -x

		# echo "${YELLOW}INFO:${NC} export config" &&
		# dd if=floppya.img of=config bs=512 skip=258 count=1 conv=notrunc &&

		if [ -f "./printer.out" ]
		then
			echo "${GREEN}From the printer:${NC}" &&
			cat ./printer.out
		else
			echo "${YELLOW}INFO:${NC} the printer was empty"
		fi
	else
		echo "${RED}ERROR:${NC} floppya.img not found"
	fi
)
