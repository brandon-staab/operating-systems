#!/bin/sh

. ./config.sh

echo "${YELLOW}INFO:${NC} compile ${GREEN}t3.c${NC}" &&
bcc -ansi -c ../assets/programs/t3/t3.c -o ../build/t3.o &&

echo "${YELLOW}INFO:${NC} link ${GREEN}t3${NC}" &&
ld86 -d ../build/t3.o ../build/interrupt.o -o ../build/t3
