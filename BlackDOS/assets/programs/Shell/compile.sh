#!/bin/sh

. ./config.sh

echo "${YELLOW}INFO:${NC} compile ${GREEN}Shell.c${NC}" &&
bcc -ansi -c ../assets/programs/Shell/Shell.c -o ../build/Shell.o &&

echo "${YELLOW}INFO:${NC} link ${GREEN}Shell${NC}" &&
ld86 -d ../build/Shell.o ../build/interrupt.o -o ../build/Shell
