#include "../syscall/blackdos.h"

int has_prefix(char* c_str, char* prefix);
int is_capital_letter(char c);
int is_user_filename(char* c_str);

void boot();
void clrs();
void ddir();
void help();
void senv();
void copy(char* c_str);
void echo(char* c_str);
void exec(char* c_str);
void move(char* c_str);
void prnt(char* c_str);
void remv(char* c_str);
void show(char* c_str);
void twet(char* c_str);


int
main() {
	int size = 192;
	char buffer[192];

	// load config
	interrupt(0x21, 0x51, 0, 0, 0);

	// main command loop
	while (1) {
		PRINTS("\r\n^(~(oo)~)^  \0");
		SCANS(buffer, size);

		if (has_prefix(buffer, "boot")) {
			boot();
		} else if (has_prefix(buffer, "clrs")) {
			clrs();
		} else if (has_prefix(buffer, "ddir")) {
			ddir();
		} else if (has_prefix(buffer, "help")) {
			help();
		} else if (has_prefix(buffer, "senv")) {
			senv();
		} else if (has_prefix(buffer, "copy ")) {
			copy(buffer + 5);
		} else if (has_prefix(buffer, "echo ")) {
			echo(buffer + 5);
		} else if (has_prefix(buffer, "exec ")) {
			exec(buffer + 5);
		} else if (has_prefix(buffer, "move ")) {
			move(buffer + 5);
		} else if (has_prefix(buffer, "prnt ")) {
			prnt(buffer + 5);
		} else if (has_prefix(buffer, "remv ")) {
			remv(buffer + 5);
		} else if (has_prefix(buffer, "show ")) {
			show(buffer + 5);
		} else if (has_prefix(buffer, "twet ")) {
			twet(buffer + 5);
		} else {
			PRINTS("bad command or file name");
		}
	}
}


int
has_prefix(char* c_str, char* prefix) {
	while (*c_str == *prefix) {
		c_str++;
		prefix++;
		if (*prefix == '\0') return 1;
	}

	return 0;
}


int
is_capital_letter(char c) {
	return 'A' <= c && c <= 'Z';
}


int
is_user_filename(char* c_str) {
	if (is_capital_letter(*c_str) || *c_str == '\0') {
		PRINTS("duplicate or invalid filename.\r\n\0");
		return 0;
	}

	return 1;
}


void
boot() {
	interrupt(0x21, 0x0B, 0, 0, 0);
}


void
clrs() {
	interrupt(0x21, 0x0C, 0, 0, 0);
}


void
ddir() {
	int i = 0;
	int file_sectors = 0;
	int user_sectors = 0;
	int system_sectors = 0;
	char* entry = 0x0;
	char buffer[16];
	char dir[0x200];

	// print labels
	PRINTS("File     | Sectors Used\r\n\0");
	PRINTS("-----------------------\r\n\0");

	// read dir sector
	interrupt(0x21, 0x02, dir, 257, 0);

	entry = dir;
	while (entry - dir < 512) {
		// skip empty directories
		if (*entry == '\0') {
			entry += 32;
			continue;
		}

		// print non-hidden files
		if (!is_capital_letter(*entry)) {
			for (i = 0; i < 8; i++) {
				buffer[i] = entry[i];
			}
			buffer[i] = '\0';
			PRINTS(buffer);

			i = 0;
			while (buffer[i] != '\0') i++;
			while (i++ < 8) PRINTC(' ');
			PRINTS(" | \0");
		}

		// count used secotrs
		for (i = 8, file_sectors = 0; i < 32; i++) {
			if (entry[i] == '\0') break;
			file_sectors++;
		}

		// update sector tallies
		if (is_capital_letter(*entry)) {
			system_sectors += file_sectors;
		} else {
			PRINTN(file_sectors);
			ENDL;

			user_sectors += file_sectors;
		}
		entry += 32;
	}

	// print summary
	PRINTS("-----------------------\r\n\0");
	PRINTN(user_sectors);
	PRINTS(" user secotr(s)\r\n\0");
	PRINTN(system_sectors);
	PRINTS(" system secotr(s)\r\n\0");
	PRINTN(255 - user_sectors - system_sectors);
	PRINTS(" available secotr(s)\r\n\0");
}


void
help() {
	interrupt(0x21, 0x04, "Help", 4, 0);
}


void
senv() {
	interrupt(0x21, 0x04, "Stenv", 4, 0);
}


void
copy(char* c_str) {
	int i;
	char src[9];
	char dest[9];
	char buffer[0x3000];

	// get src filename
	for (i = 0; i < 8 && *c_str != ' '; i++, c_str++) {
		src[i] = *c_str;
	}
	src[i] = '\0';

	// reject restricted filenames
	if (!is_user_filename(src)) return;

	// skip space
	c_str++;

	// get dest filename
	for (i = 0; i < 8 && *c_str != '\0'; i++, c_str++) {
		dest[i] = *c_str;
	}
	dest[i] = '\0';

	// reject restricted filenames
	if (!is_user_filename(dest)) return;

	// read file
	interrupt(0x21, 0x03, src, buffer, &i);

	// write file
	interrupt(0x21, 0x08, dest, buffer, i);
}


void
echo(char* c_str) {
	PRINTS(c_str);
}


void
exec(char* c_str) {
	interrupt(0x21, 0x04, c_str, 4, 0);
}


void
move(char* c_str) {
	int i;
	char* entry;
	char src[9];
	char dest[9];
	char dir[0x200];
	char buffer[0x3000];

	// get src
	for (i = 0; i < 8 && *c_str != ' '; i++, c_str++) {
		src[i] = *c_str;
	}

	// null terminate
	while (i < 9) {
		src[i++] = '\0';
	}

	// reject restricted filenames
	if (!is_user_filename(src)) return;

	// skip space
	c_str++;

	// get dest
	for (i = 0; i < 8 && *c_str != '\0'; i++, c_str++) {
		dest[i] = *c_str;
	}

	// null terminate
	while (i < 9) {
		dest[i++] = '\0';
	}

	// reject restricted filenames
	if (!is_user_filename(dest)) return;

	// read dir sector
	interrupt(0x21, 0x02, dir, 257, 0);

	// find the entry
	entry = dir;
	while (entry - dir < 512) {
		while (entry[i] == src[i]) {
			if (src[i] == '\0' || i == 7) goto rename;
			i++;
		}

		entry += 32;
		i = 0;
	}

	PRINTS("file not found.\r\n\0");
	return;  // don't save changes

rename:
	for (i = 0; i < 8; i++) {
		entry[i] = dest[i];
	}

	// write dir sector
	interrupt(0x21, 0x06, dir, 257, 0);
}


void
prnt(char* c_str) {
	int size;
	char buffer[0x3000];

	interrupt(0x21, 0x03, c_str, buffer, &size);
	LPRINTS(buffer);
}


void
remv(char* c_str) {
	// reject restricted filenames
	if (!is_user_filename(c_str)) return;

	interrupt(0x21, 0x07, c_str, 0, 0);
}


void
show(char* c_str) {
	int size;
	char buffer[0x3000];

	interrupt(0x21, 0x03, c_str, buffer, &size);
	PRINTS(buffer);
}


void
twet(char* c_str) {
	char buffer[0x200];

	// reject restricted filenames
	if (!is_user_filename(c_str)) return;

	// store twet
	SCANS(buffer, 0x200);
	interrupt(0x21, 0x08, c_str, buffer, 1);
}
