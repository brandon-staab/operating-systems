/** Fibonacci
 *  Dr. Tim O'Neil, 2018
 *  Brandon Staab, 2019
 */

#include "../syscall/blackdos.h"


void
main() {
	int i, a = 1, b = 1, c, n;

	PRINTS("How many terms? \0");
	SCANN(n);

	if (n < 3) n = 3;
	if (n > 23) n = 23;
	ENDL;

	PRINTN(n);
	PRINTS(" terms: \0");
	PRINTN(a);
	PRINTS(" \0");
	PRINTN(b);
	PRINTS(" \0");

	for (i = 0; i < n - 2; i++) {
		c = a + b;
		PRINTN(c);
		PRINTS(" \0");
		a = b;
		b = c;
	}

	ENDL;
	BUSY_STOP;
}
