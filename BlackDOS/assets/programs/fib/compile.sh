#!/bin/sh

. ./config.sh

echo "${YELLOW}INFO:${NC} compile ${GREEN}fib.c${NC}" &&
bcc -ansi -c ../assets/programs/fib/fib.c -o ../build/fib.o &&

echo "${YELLOW}INFO:${NC} link ${GREEN}fib${NC}" &&
ld86 -d ../build/fib.o ../build/interrupt.o -o ../build/fib
