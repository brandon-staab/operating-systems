#include "../syscall/blackdos.h"


void
main() {
	PRINTS(
		"Help:\r\n"
		"-------------------------------------------------------------\r\n"
		"boot                           - reboot the system\r\n"
		"clrs                           - clear the terminal screen\r\n"
		"copy <source> <destination>    - copy file\r\n"
		"ddir                           - list disk directory contents\r\n"
		"echo <comment>                 - display a line of text\r\n"
		"help                           - display the user manual\r\n"
		"move <source> <destination>    - move a file\r\n"
		"prnt <filename>                - prints file on printer\r\n"
		"remv <filename>                - remove file\r\n"
		"senv                           - set environment variables\r\n"
		"show <filename>                - prints file on terminal\r\n"
		"twet <filename>                - writes twet to file\r\n\0");

	BUSY_STOP
}
