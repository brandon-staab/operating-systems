#!/bin/sh

. ./config.sh

echo "${YELLOW}INFO:${NC} compile ${GREEN}Help.c${NC}" &&
bcc -ansi -c ../assets/programs/Help/Help.c -o ../build/Help.o &&

echo "${YELLOW}INFO:${NC} link ${GREEN}Help${NC}" &&
ld86 -d ../build/Help.o ../build/interrupt.o -o ../build/Help
