/** Calendar
 *  Dr. Tim O'Neil, 2018
 *  Brandon Staab, 2019
 */

#include "../syscall/blackdos.h"


int first_of_month(int, int);
int is_leap_year(int);


void
main() {
	int mo, day, yr, no_days, i, c;

	mo = -1;
	while (mo < 1 || mo > 12) {
		PRINTS("Enter desired month: \0");
		SCANN(mo);
	}

	PRINTS("Enter desired year: \0");
	SCANN(yr);

	if (yr < 100) yr += 2000;
	day = first_of_month(mo, yr);
	ENDL;

	switch (mo) {
		case 1:
			PRINTS("January \0");
			no_days = 31;
			break;
		case 2:
			PRINTS("February \0");
			no_days = 28 + is_leap_year(yr);
			break;
		case 3:
			PRINTS("March \0");
			no_days = 31;
			break;
		case 4:
			PRINTS("April \0");
			no_days = 30;
			break;
		case 5:
			PRINTS("May \0");
			no_days = 31;
			break;
		case 6:
			PRINTS("June \0");
			no_days = 30;
			break;
		case 7:
			PRINTS("July \0");
			no_days = 31;
			break;
		case 8:
			PRINTS("August \0");
			no_days = 31;
			break;
		case 9:
			PRINTS("September \0");
			no_days = 30;
			break;
		case 10:
			PRINTS("October \0");
			no_days = 31;
			break;
		case 11:
			PRINTS("November \0");
			no_days = 30;
			break;
		default:
			PRINTS("December \0");
			no_days = 31;
	}

	PRINTN(yr);
	PRINTS("\r\n\0");
	PRINTS("Su Mo Tu We Th Fr Sa\r\n\0");
	PRINTS("====================\r\n\0");

	for (i = 1; i <= day; i++) PRINTS("   \0");
	for (i = 1, c = day + 1; i <= no_days; i++, c++) {
		if (i < 10) PRINTS(" \0");
		PRINTN(i);
		PRINTS(" \0");

		if (c == 7) {
			c = 0;
			ENDL;
		}
	}

	ENDL;
	BUSY_STOP;
}


int
mod(int a, int b) {
	int x = a;
	while (x >= b) x = x - b;
	return x;
}


int
first_of_month(int m, int y) {
	int a, b, c, e, f, g, h, w, z;
	a = (14 - m) / 12;
	b = y - a;
	c = b / 4;
	e = b / 100;
	f = b / 400;
	g = m + 12 * a - 2;
	h = (31 * g) / 12;
	w = b + c + 1 - e + f + h;
	z = mod(w, 7);
	return z;
}


int
is_leap_year(int y) {
	int a, b, c;
	a = mod(y, 4);
	b = mod(y, 100);
	c = mod(y, 400);
	if ((a == 0) && ((b != 0) || (c == 0)))
		return 1;
	else
		return 0;
}
