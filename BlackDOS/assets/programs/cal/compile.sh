#!/bin/sh

. ./config.sh

echo "${YELLOW}INFO:${NC} compile ${GREEN}cal.c${NC}" &&
bcc -ansi -c ../assets/programs/cal/cal.c -o ../build/cal.o &&

echo "${YELLOW}INFO:${NC} link ${GREEN}cal${NC}" &&
ld86 -d ../build/cal.o ../build/interrupt.o -o ../build/cal
