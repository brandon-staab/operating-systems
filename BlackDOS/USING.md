Using
=====

Using BlackDOS one-liner
------------------------
1. Navigate to the `dev` directory.
2. Ensure the correct lab to be compiled is selected in `config.sh`.
3. Run the `all` script that builds, runs, and then prints the printer output upon completion.

```shell
cd ./dev/
grep '^LAB=' ./config.sh
./all.sh
```

Building
--------
1. Navigate to the `dev` directory.
2. Ensure the correct lab to be compiled is selected in `config.sh`.
3. Compile on a machine with the correct tool chain.

```shell
cd ./dev/
grep '^LAB=' ./config.sh
./compile_os.sh
```

Running
-------
1. Navigate to the `dev` directory.
2. Check that BlackDOS is built
3. Run BlackDOS on Bochs.

```shell
cd ./dev/
ls ../run/
./run.sh
```

If floppya.img is not present in the run directory, see `Building`.
