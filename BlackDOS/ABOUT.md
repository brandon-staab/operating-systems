About
=====

File System
-----------
Each sector is `512 bytes`.  The map is `256 bytes`.  The directory is `512 bytes` and can store up to `16 files`.  Each file can take up `24 sectors` or `12 Kibibytes`.  The maximum sum of all files is `255 sectors` or `127.5 Kibibytes`.  This is because the first sector is reserved for the boot loader and we can only represent `256` distinct values with a byte.  The maximum filename length is `8 characters` long.


Color Table
-----------
-   0  - Black
-   1  - Blue
-   2  - Green
-   3  - Cyan
-   4  - Red
-   5  - Magenta
-   6  - Brown
-   7  - White / Light Gray
-   8  - Gray / Dark Gray
-   9  - Light Blue
-   10 - Light Green
-   11 - Light Cyan
-   12 - Light Red
-   13 - Light Magenta
-   14 - Yellow
-   15 - Bright White

The background can only be set to a color code in the range `zero` to `seven`.  The foreground can be set to any color in the range `zero` to `fifteen`.


Interrupt Table
---------------
#### 0x00 - Print String
#### 0x01 - Read String
#### 0x02 - Read Sector
#### 0x03 - Read File
#### 0x06 - Write Sector
#### 0x07 - Delete File
#### 0x08 - Write File
#### 0x0C - Clear Screen
#### 0x0D - Write Int
#### 0x0E - Read Int
#### 0x0F - Error
- 0x00 - File not found
- 0x01 - Bad file name
- 0x02 - Disk full
- 0x40 - Division by zero
- 0x41 - Integer overflow
- Default - General error

#### 0x40 - Print Char
#### 0x41 - Print Int as Binary Number
#### 0x42 - Print End Line
#### Default - General BlackDOS error


Commands
--------
* boot                           - reboot the system
* clrs                           - clear the terminal screen
* copy <source> <destination>    - copy file
* ddir                           - list disk directory contents
* echo <comment>                 - display a line of text
* help                           - display the user manual
* move <source> <destination>    - move a file
* prnt <filename>                - prints file on printer
* remv <filename>                - remove file
* senv                           - set environment variables
* show <filename>                - prints file on terminal
* twet <filename>                - writes twet to file
