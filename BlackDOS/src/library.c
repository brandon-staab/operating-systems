#include "library.h"

#include "kernel.h"


void
print_welcome() {
	int size;
	char filename[8];
	char buffer[0xC00];

	filename[0] = 'W';
	filename[1] = 'e';
	filename[2] = 'l';
	filename[3] = 'c';
	filename[4] = 'o';
	filename[5] = 'm';
	filename[6] = 'e';
	filename[7] = '\0';

	read_file(filename, buffer, &size);
	print_string(buffer, 0);
}


// Precondition: num must be atleast 7 elements
void
int_to_string(int i, char* num) {
	int num_idx = 0;
	int temp_idx = 0;
	char temp[5];
	int results[2];

	/* Debug */
	// print_char('[', 0);
	// print_int_hex(i);
	// print_char(']', 0);

	// add negative sign if necessary
	if (i < 0) {
		num[num_idx++] = '-';
		i = -i;
	}

	// calculate digits
	do {
		divide(i, 10, results);

		// integer overflow
		if (results[0] == 0 && results[1] == 0 && i != 0) {
			num[0] = '\n';
			return;
		}

		temp[temp_idx++] = results[1] + '0';
		i = results[0];
	} while (i != 0);

	// null terminate
	num[num_idx + temp_idx] = '\0';

	// place digits in correct order
	do {
		num[num_idx++] = temp[--temp_idx];
	} while (temp_idx > 0);
}


char*
dir_lookup(char* fname, char* directory, char** free) {
	char* entry = directory;
	int found_freespcae = 0;
	int idx = 0;
	*free = 0x0;

	while (entry - directory < 512) {
		if (*entry == '\0') {
			if (!found_freespcae) {
				found_freespcae = 1;
				*free = entry;
			}
		} else {
			while (entry[idx] == fname[idx]) {
				if (fname[idx] == '\0' || idx == 7) return entry;
				idx++;
			}
		}

		entry += 32;
		idx = 0;
	}

	return 0x0;
}


int
find_free_sector(char* map) {
	int i = 0;

	for (i = 0; i < 256; i++) {
		if (map[i] == 0x00) return i;
	}

	return 0;
}


void
get_rel_floppy_sec4bios(int abs_sec_num, int device_num, int* output) {
	abs2rel_floppy_sec(abs_sec_num, output);

	output[0] = (output[0] << 8) + output[2];
	output[1] = (output[1] << 8) + device_num;
}


void
abs2rel_floppy_sec(int abs_sec_num, int* output) {
	int results[2];
	divide(abs_sec_num, 18, results);

	output[0] = div(abs_sec_num, 36);  // track_num = abs_sec_num % 36
	output[1] = mod(results[0], 2);    // head_num = ( abs_sec_num / 18 ) % 2
	output[2] = results[1] + 1;        // rel_sec_no = abs_sec_num % 18 + 1
}


int
div(int numerator, int denominator) {
	int results[2];
	divide(numerator, denominator, results);

	return results[0];
}


int
mod(int numerator, int denominator) {
	int results[2];
	divide(numerator, denominator, results);

	return results[1];
}


void
divide(int numerator, int denominator, int* results) {
	// static int overflow = 0x7FFF + 1;
	int overflow = 0x7FFF + 1;

	if (numerator == overflow || denominator == overflow) {
		interrupt(0x21, 0x0F, 0x41, 0, 0);
		results[0] = 0;
		results[1] = 0;
		return;
	}

	if (denominator == 0) {
		interrupt(0x21, 0x0F, 0x40, 0, 0);
		results[0] = 0;
		results[1] = 0;
		return;
	}

	if (denominator < 0) {
		divide(numerator, -denominator, results);
		results[0] = -results[0];
		return;
	}

	if (numerator < 0) {
		divide(-numerator, denominator, results);
		results[0] = -results[0];
		if (results[1] != 0) {
			results[0] -= 1;
			results[1] = denominator - results[1];
		}
		return;
	}

	divide_unsigned(numerator, denominator, results);
}


void
divide_unsigned(int numerator, int denominator, int* results) {
	results[0] = 0;
	results[1] = numerator;

	while (results[1] >= denominator) {
		results[0] = results[0] + 1;
		results[1] = results[1] - denominator;
	}
}
