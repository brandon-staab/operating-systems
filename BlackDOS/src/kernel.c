#include "kernel.h"

#include "library.h"


void
print_string(char* c_str, int dest) {
	while (*c_str != '\0') {
		print_char(*c_str, dest);
		c_str++;
	}
}


void
read_string(char* buffer, int size) {
	char* cursor = buffer;
	char* max_cursor = buffer + size - 1;  // save space for null terminator

	// exit early if no space in buffer
	if (max_cursor <= cursor) {
		buffer[0] = '\0';
		error(0x50);
		return;
	}

	while (1) {
		read_char(cursor);  // store key press at the cursor
		switch (*cursor) {
			case 0x08:                    // backspace
				if (cursor != buffer) {   // not first element in buffer
					cursor--;             // move cursor pointer back in buffer
					print_char(0x08, 0);  // move cursor back on teletype output
				}
				break;
			case 0x0D:  // enter
				*cursor = '\0';
				print_endl(0);
				return;
			default:
				if (cursor == max_cursor) continue;  // prevent overflow
				print_char(*cursor++, 0);            // print pressed char and move cursor
		}
	}
}


void
read_sector(char* c_str, int abs_sec_num, int device_num) {
	int output[3];

	get_rel_floppy_sec4bios(abs_sec_num, device_num, output);
	interrupt(0x13, 0x0201, c_str, output[0], output[1]);
}


void
read_file(char* fname, char* buffer, int* size) {
	char* entry = 0x0;
	char* end = 0x0;
	char directory[512];

	if (*fname == '\0') return error(0x01);        // quick reject invalid file name
	read_sector(directory, 257, 0);                // wait until after quick rejects
	entry = dir_lookup(fname, directory, &entry);  // find in directory
	if (entry == 0x0) return error(0x00);          // file not found

	// position for sector area
	entry += 8;
	end = entry + 24;

	// load sectors into buffer
	while (entry < end) {
		if (*entry != 0) {
			read_sector(buffer, *entry, 0);

			entry++;
			buffer += 512;
			continue;
		}

		// return number of sectors read
		*size = 24 - (end - entry);
		return;
	}
}


void
run_program(char* name, int segment) {
	char buffer[0x3000];
	int size;

	// invalid segment number
	if (segment < 2 || segment > 9) return error(0x11);

	read_file(name, buffer, &size);

	// get offsets
	segment *= 0x1000;
	size *= 0x200;

	// program does not fit in segment space
	if (segment + size > 0x9FFF) return error(0x12);
	// put buffered data into segment
	while (size >= 0) {
		put_in_memory(segment, size, *(buffer + size));
		size--;
	}

	launch_program(segment);
}


void
stop() {
	// launch the shell in sector 2
	launch_program(0x2000);
}


void
write_sector(char* c_str, int abs_sec_num, int device_num) {
	int output[3];

	get_rel_floppy_sec4bios(abs_sec_num, device_num, output);
	interrupt(0x13, 0x0301, c_str, output[0], output[1]);
}


void
delete_file(char* fname) {
	int i = 0;
	char* entry = 0x0;
	char map[512];
	char directory[512];


	if (*fname == '\0') return error(0x01);        // quick reject invalid file name
	read_sector(directory, 257, 0);                // wait until after quick rejects
	entry = dir_lookup(fname, directory, &entry);  // find in directory
	if (entry == 0x0) return error(0x00);          // file not found

	*entry = '\0';                    // remove entry from directory by nulling first char
	write_sector(directory, 257, 0);  // save change

	// update map
	read_sector(map, 256, 0);
	for (i = 8; i < 24; i++) {
		if (entry[i] == '\0') break;
		map[(int)entry[i]] = 0x00;  // free sector
	}
	write_sector(map, 256, 0);
}


void
write_file(char* fname, char* buffer, int num_sectors) {
	char* entry = 0x0;
	int i = 0;
	int s = 0;
	char map[512];
	char directory[512];

	if (*fname == '\0') return error(0x01);    // quick reject invalid file name
	if (num_sectors > 24) return error(0x04);  // quick reject file too large
	read_sector(directory, 257, 0);            // wait until after quick rejects

	// look up free entry for file and save its name in directory
	if (dir_lookup(fname, directory, &entry) != 0x0) return error(0x05);  // error on duplicate file name
	if (entry == 0x0) return error(0x03);                                 // error on directory full
	for (i = 0; i < 8 && fname[i] != '\0'; i++) entry[i] = fname[i];      // copy filename into the entry
	while (i < 8) entry[i++] = 0;                                         // pad with zeros
	entry += 8;                                                           // move to sector record of entry

	read_sector(map, 256, 0);  // wait until after quick rejects

	// write sectors to disk and update filesystem
	for (i = 0; i < num_sectors; i++) {
		s = find_free_sector(map);
		if (!s) return error(0x02);  // insufficient disk space
		map[s] = 0xFF;               // mark sector as used
		entry[i] = s;                // record sector in directory
		write_sector(buffer, s, 0);
		buffer += 512;
	}

	// pad remaining entry with zeros
	while (i < 24) entry[i++] = 0;

	// write changes back to disk
	write_sector(map, 256, 0);
	write_sector(directory, 257, 0);
}


void
reboot() {
	interrupt(0x19, 0, 0, 0, 0);
}


/**
 * WARNING: expects color codes to be one greater than value in table
 *
 * Color Table
 *   0  - Black
 *   1  - Blue
 *   2  - Green
 *   3  - Cyan
 *   4  - Red
 *   5  - Magenta
 *   6  - Brown
 *   7  - White / Light Gray
 *   8  - Gray / Dark Gray
 *   9  - Light Blue
 *   10 - Light Green
 *   11 - Light Cyan
 *   12 - Light Red
 *   13 - Light Magenta
 *   14 - Yellow
 *   15 - Bright White
 *
 * [0-7]  -> Background
 * [0-15] -> Foreground
 */
void
clear_scren(int bg_color, int fg_color) {
	int i;

	// clear screen
	for (i = 0; i < 24; i++) {
		print_endl(0);
	}

	// reset cursor
	interrupt(0x10, 0x0200, 0, 0, 0);

	// set colors
	if (bg_color > 0 && bg_color <= 8 && fg_color > 0 && fg_color <= 16) {
		interrupt(0x10, 0x0600, ((bg_color - 1) << 12) + ((fg_color - 1) << 8), 0x0000, 0x184F);  // 0x184F -> (24, 79)
	}
}


void
write_int(int i, int dest) {
	char num[7];

	int_to_string(i, num);
	print_string(num, dest);
}


void
read_int(int* i_ptr) {
	int x;
	int idx;
	char buffer[7];
	char num[7];

start:
	// reset
	*i_ptr = 0;
	idx = 0;

	read_string(buffer, 7);  // get input

	// convert string to int
	if (buffer[0] == '-') idx++;           // process negative
	while (buffer[idx] != '\0') {          // loop over string
		x = buffer[idx] - '0';             // get digit
		if (x < 0 || x > 9) goto restart;  // digit validation

		// inset digit
		*i_ptr *= 10;
		*i_ptr += x;

		idx++;
	}

	if (buffer[0] == '-') *i_ptr = -*i_ptr;  // negative numbers
	if (idx >= 5) {                          // input validation
		if (buffer[0] == '-') {              // quick check length
			if (idx > 6) goto restart;
		} else {
			if (idx > 5) goto restart;
		}

		// make sure it didn't overflow
		int_to_string(*i_ptr, num);
		for (idx = 0; idx < 7; idx++) {
			if (num[idx] != buffer[idx]) goto restart;
			if (num[idx] == '\0') return;
		}
	}

	return;

restart:
	print_string("Integer input must be in the domain [-32767, 32767].\r\n\0", 0);
	goto start;
}


void
error(int code) {
	/* Debug */
	// print_char('<', 0);
	// print_char('E', 0);
	// print_int_hex(code);
	// print_char('>', 0);

	switch (code) {
		case 0x00:
			print_string("File not found.\r\n\0", 0);
			break;
		case 0x01:
			print_string("Bad filename.\r\n\0", 0);
			break;
		case 0x02:
			print_string("Disk full.\r\n\0", 0);
			break;
		case 0x03:
			print_string("Filesystem directory full.\r\n\0", 0);
			break;
		case 0x04:
			print_string("File is too large.\r\n\0", 0);
			break;
		case 0x05:
			print_string("Duplicate filename.\r\n\0", 0);
			break;
		case 0x10:
			print_string("Unknown BlackDOS interrupt\0", 0);
			break;
		case 0x11:
			print_string("Invalid segment number.\r\n\0", 0);
			break;
		case 0x12:
			print_string("Program does not fit in segment space.\r\n\0", 0);
			break;
		case 0x40:
			print_string("Division by zero.\r\n\0", 0);
			break;
		case 0x41:
			print_string("Integer overflow.\r\n\0", 0);
			break;
		case 0x50:
			print_string("Buffer of size zero.\r\n\0", 0);
			break;
		default:
			print_string("General error.\r\n\0", 0);
			break;
	}

	stop();
}


void
print_char(char c, int dest) {
	/* Debug */
	// dest = 1;

	if (dest == 0) {
		// teletype output
		interrupt(0x10, (0x000E << 8) + c, 0, 0, 0);
	} else if (dest == 1) {
		// parallel port output
		interrupt(0x17, c, 0, 0, 0);
	}
}


void
print_bin(int i, int dest) {
	char buffer[17];
	int idx = 16;

	buffer[idx] = '\0';
	while (idx--) {  // put binary representation in buffer
		buffer[idx] = (i & 1) + '0';
		i >>= 1;
	}

	print_string(buffer, dest);
}


void
print_endl(int dest) {
	// print_string("\r\n\0", dest);
	print_char('\r', dest);
	print_char('\n', dest);
}


void
read_char(char* c) {
	*c = interrupt(0x16, 0, 0, 0, 0);
}


void
read_press() {
	char c;
	read_char(&c);
}


void
setup() {
	make_interrupt_21();
	load_config();
}


void
load_config() {
	char buffer[0x200];

	// load config
	interrupt(0x21, 0x02, buffer, 258, 0);

	// set config
	interrupt(0x21, 0x0C, buffer[0] + 1, buffer[1] + 1, 0);

	print_welcome();
}


void
handle_interrupt_21(int ax, int bx, int cx, int dx) {
	/* Debug */
	// print_char('<', 0);
	// print_char('I', 0);
	// print_int_hex(ax);
	// // print_char(',', 0);
	// // print_int_hex(bx);
	// // print_char(',', 0);
	// // print_int_hex(cx);
	// // print_char(',', 0);
	// // print_int_hex(dx);
	// print_char('>', 0);

	switch (ax) {
		case 0x00:
			return print_string(bx, cx);
		case 0x01:
			return read_string(bx, cx);
		case 0x02:
			return read_sector(bx, cx, dx);
		case 0x03:
			return read_file(bx, cx, dx);
		case 0x04:
			return run_program(bx, cx);
		case 0x05:
			return stop();
		case 0x06:
			return write_sector(bx, cx, dx);
		case 0x07:
			return delete_file(bx);
		case 0x08:
			return write_file(bx, cx, dx);
		case 0x0B:
			return reboot();
		case 0x0C:
			return clear_scren(bx, cx);
		case 0x0D:
			return write_int(bx, cx);
		case 0x0E:
			return read_int(bx);
		case 0x0F:
			return error(bx);
		case 0x40:
			return print_char(bx, cx);
		case 0x41:
			return print_bin(bx, cx);
		case 0x42:
			return print_endl(bx);
		case 0x48:
			return read_char(bx);
		case 0x49:
			return read_press();
		case 0x50:
			return setup();
		case 0x51:
			return load_config();
		default:
			return error(0x10);
	}
}
