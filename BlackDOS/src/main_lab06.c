#include "kernel.h"
#include "library.h"


int
main() {
	char buffer[0x200];

	setup();

	interrupt(0x21, 0x04, "Shell\0", 2, 0);
	interrupt(0x21, 0, "Bad or missing command interpreter.\r\n\0", 0, 0);

	stop();
}
