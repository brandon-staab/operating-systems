; bootload.asm
;
; Michael Black, 2007
; Brandon Staab, 2019
;
; This is a simple bootloader that loads and executes a kernel at sector 259

		bits 16
KSEG	equ 0x1000		; kernel goes into memory at 0x1000
KSIZE	equ 20			; kernel is at most 20 sectors (and probably less)
		org 0h			; boot loader starts at 0 in segment 0x7c00

; put the segment registers for the kernel at KSEG:0 which is 0x1000
	mov ax, KSEG	; set ax to KSEG
	mov ds, ax		; set up the data segment register
	mov ss, ax		; set up the stack segment register
	mov es, ax		; set up the extra segment register

; have the stack start at KSEG:fff0
	mov ax, 0xfff0	; set ax to 0xfff0
	mov sp, ax		; set the stack pointer register
	mov bp, ax		; set the base pointer register

; read in the kernel from the disk at sector 259
	mov ch, 7		; ch holds track number
	mov cl, 8		; cl holds sector number
	mov dh, 0		; dh holds head number

	mov ah, 2		; absolute disk read
	mov al, KSIZE	; read KSIZE sectors
	mov dl, 0		; read from device 0 which is floppy disk A
	mov bx, 0		; read data into address 0 (in the segment)
	int 13h			; call BIOS disk read function

	jmp KSEG:0		; call the kernel

	times 510-($-$$) db 0	; pad sector with zeros
	dw 0xAA55				; AA55 tells BIOS that this is a valid bootloader
