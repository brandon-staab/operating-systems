#include "kernel.h"
#include "library.h"


int
main() {
	setup();

	// Launch a program
	interrupt(0x21, 0x04, "kitty1\0", 2, 0);
	// interrupt(0x21, 0x04, "kitty2\0", 3, 0);
	// interrupt(0x21, 0x04, "Stenv\0", 4, 0);
	// interrupt(0x21, 0x04, "cal\0", 5, 0);
	// interrupt(0x21, 0x04, "fib\0", 6, 0);
	// interrupt(0x21, 0x04, "t3\0", 7, 0);

	// interrupt(0x21, 0x04, "t3\0", 9, 0);
	interrupt(0x21, 0x00, "Error if this executes.\r\n\0", 0, 0);
	stop();
}
