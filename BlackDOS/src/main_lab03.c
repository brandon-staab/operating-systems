/**
 * compile_os.sh:
 *   echo "${YELLOW}INFO:${NC} add msg to disk at sector 30" &&
 *   dd if=msg of=floppya.img bs=512 count=1 seek=30 conv=notrunc
 */

/** msg **
|\      _,,,---,,_
/,`.-'`'    -.  ;-;;,_
|,4-  ) )-,_..;\ (  `'-'
'---''(_/--'  `-'\_)
Soft Kitty, Warm Kitty,
little ball of fur.
Happy Kitty, Sleepy Kitty,
		 purr, purr, purr.
*********/

#include "kernel.h"
#include "library.h"


int
main() {
	char buffer[512];
	int i;

	make_interrupt_21();

	// zero out buffer
	for (i = 0; i < 512; i++) {
		buffer[i] = 0;
	}

	buffer[0] = 2;   // background
	buffer[1] = 15;  // foreground

	// write buffer to sector
	interrupt(0x21, 0x06, buffer, 258, 0);

	// clear screen and set foreground and background colors
	interrupt(0x21, 0x0C, buffer[0] + 1, buffer[1] + 1, 0);

	print_logo();

	// read then print sector 30 - NOTE: must put file in sector 30
	interrupt(0x21, 0x02, buffer, 30, 0);
	interrupt(0x21, 0x00, buffer, 0, 0);

	stop();
}
