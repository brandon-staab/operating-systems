#ifndef library_h
#define library_h


void print_welcome();
void int_to_string(int i, char* num);  // NOTE: num has precondition

char* dir_lookup(char* fname, char* directory, char** free);
int find_free_sector(char* map);

void get_rel_floppy_sec4bios(int abs_sec_num, int device_num, int* output);
void abs2rel_floppy_sec(int abs_sec_num, int* output);

int div(int numerator, int denominator);
int mod(int numerator, int denominator);
void divide(int numerator, int denominator, int* results);
void divide_unsigned(int numerator, int denominator, int* results);

#endif /* end of include guard: library_h */
