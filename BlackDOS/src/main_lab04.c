#include "kernel.h"
#include "library.h"


int
main() {
	char buffer[0x3000];
	int size;

	/* Step 0 - make interrupt 0x21, setup config file, and print logo */
	setup();

	/* Step 1 - load/edit/print file */
	interrupt(0x21, 0x03, "spc03\0", buffer, &size);
	buffer[7]  = '2';
	buffer[8]  = '0';
	buffer[9]  = '1';
	buffer[10] = '9';
	interrupt(0x21, 0x00, buffer, 0, 0);
	print_endl(0);

	/* Step 2 - write revised file */
	interrupt(0x21, 0x08, "spr19\0", buffer, size);

	/* Step 3 - delete original file */
	interrupt(0x21, 0x07, "spc03\0", 0, 0);

	stop();
}
