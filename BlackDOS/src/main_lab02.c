#include "kernel.h"
#include "library.h"


void
main() {
	char food[25], adjective[25], color[25], animal[25];
	int temp;

	setup();

	interrupt(0x21, 0x00, "\r\nWelcome to the Mad Libs kernel.\r\n\0", 0, 0);
	interrupt(0x21, 0x00, "Enter a food: \0", 0, 0);
	interrupt(0x21, 0x1, food, 25, 0);

	temp = 0;
	while ((temp < 100) || (temp > 120)) {
		interrupt(0x21, 0x00, "Enter a number between 100 and 120: \0", 0, 0);
		interrupt(0x21, 0xE, &temp, 0, 0);
	}

	interrupt(0x21, 0x00, "Enter an adjective: \0", 0, 0);
	interrupt(0x21, 0x01, adjective, 25, 0);

	interrupt(0x21, 0x00, "Enter a color: \0", 0, 0);
	interrupt(0x21, 0x01, color, 25, 0);

	interrupt(0x21, 0x00, "Enter an animal: \0", 0, 0);
	interrupt(0x21, 0x01, animal, 25, 0);

	interrupt(0x21, 0x00, "Your note is on the printer, go get it.\r\n\0", 0, 0);

	interrupt(0x21, 0x00, "Dear Professor O\'Neil,\r\n\0", 1, 0);
	interrupt(0x21, 0x00, "\r\nI am so sorry that I am unable to turn in my program at this time.\r\n\0", 1, 0);
	interrupt(0x21, 0x00, "First, I ate a rotten \0", 1, 0);
	interrupt(0x21, 0x00, food, 1, 0);
	interrupt(0x21, 0x00, ", which made me turn \0", 1, 0);
	interrupt(0x21, 0x00, color, 1, 0);
	interrupt(0x21, 0x00, " and extremely ill.\r\n\0", 1, 0);
	interrupt(0x21, 0x00, "I came down with a fever of \0", 1, 0);
	interrupt(0x21, 0x0D, temp, 1, 0);
	interrupt(0x21, 0x00, ". Next my \0", 1, 0);
	interrupt(0x21, 0x00, adjective, 1, 0);
	interrupt(0x21, 0x00, " pet \0", 1, 0);
	interrupt(0x21, 0x00, animal, 1, 0);
	interrupt(0x21, 0x00, " must have\r\nsmelled the remains of the \0", 1, 0);
	interrupt(0x21, 0x00, food, 1, 0);
	interrupt(0x21, 0x00, " on my computer, because he ate it. I am\r\n\0", 1, 0);
	interrupt(0x21, 0x00, "currently rewriting the program and hope you will accept it late.\r\n\0", 1, 0);
	interrupt(0x21, 0x00, "\r\nSincerely,\r\n\0", 1, 0);
	interrupt(0x21, 0x00, "(your name here)\r\n\0", 1, 0);

	stop();
}
