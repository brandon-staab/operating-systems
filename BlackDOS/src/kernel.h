#ifndef kernel_h
#define kernel_h


void print_string(char* c_str, int dest);
void read_string(char* buffer, int size);
void read_sector(char* c_str, int abs_sec_num, int device_num);
void read_file(char* fname, char* buffer, int* size);
void run_program(char* name, int segment);
void stop();
void write_sector(char* c_str, int abs_sec_num, int device_num);
void delete_file(char* fname);
void write_file(char* fname, char* buffer, int num_sectors);
void reboot();
void clear_scren(int bg_color, int fg_color);
void write_int(int i, int dest);
void read_int(int* i_ptr);
void error(int code);

void print_char(char c, int dest);
void print_bin(int i, int dest);
void print_endl(int dest);
void read_char(char* c);
void read_press();

void setup();
void load_config();

void print_ax_hex();
void print_int_hex(int i);
void put_in_memory(int* segment_base, int offset, char c);
void launch_program(int* segment_base);
int interrupt(int number, int ax, int bx, int cx, int dx);
void make_interrupt_21();
void handle_interrupt_21(int ax, int bx, int cx, int dx);

#endif /* end of include guard: kernel_h */
