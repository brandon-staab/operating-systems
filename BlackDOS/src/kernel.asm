; kernel.asm
;
; Michael Black, 2007
; Brandon Staab, 2019


; kernel.asm - contains assembly functions that you can use in your kernel
	.global _put_in_memory
	.global _launch_program
	.global _interrupt
	.global _make_interrupt_21
	.extern _handle_interrupt_21
	.global _print_int_hex
	.global _print_ax_hex


; void put_in_memory(int* segment_base, int offset, char c)
_put_in_memory:
	push bp				; save base pointer register state on stack
	mov bp, sp			; change base pointer to get arguments off the stack
	push ds				; save data segment register state on stack

	mov ax, [bp + 4]	; set AX to the segment_base pointer argument
	mov si, [bp + 6]	; set source index register to the offset argument
	mov cl, [bp + 8]	; set CL to the c argument

	mov ds, ax			; set data segment to the segment_base pointer argument
	mov [si], cl		; set memory at the offset to the c argument

	pop ds				; restore data segment register state from stack
	pop bp				; restore base pointer register state from stack
	ret


; This is called to start a program that is loaded into memory
; void launch_program(int* segment_base)
_launch_program:
	; get the segment into bx
	mov bp, sp				; change base pointer to get arguments off the stack
	mov bx, [bp + 2]		; set bx to the segment_base pointer argument

	mov ax, cs				; modify the jmp below to jump to our segment
	mov ds, ax				; this is self-modifying code
	mov si, #jump			; set source index register to #jump label
	mov [si + 3], bx		; change the first 0000 to the segment

	; set up registers with address of the segment base
	mov ds, bx				; set data segment register
	mov ss, bx				; set stack segment register
	mov es, bx				; set extra segment register

	mov sp, #0xfff0			; set up the stack pointer
	mov bp, #0xfff0			; set up the base pointer
jump:
	jmp #0x0000:0x0000		; and start running (the first 0000 is changed above)


; This sets up the interrupt 0x21 vector. When an interrupt 0x21 is called in
; the future, interrupt_21_service_routine() will run.
; void make_interrupt_21()
_make_interrupt_21:
	push ds				; save data segment register state on stack

	mov ax, #0			; interrupts are in lowest memory, store its address in register AX
	mov ds, ax			; set data segment register to the lowest memory

	mov si, #0x84		; interrupt 0x21 vector (21 * 4 = 84)

	mov ax, cs			; have interrupt go to the current segment
	mov [si + 2], ax	; set the memory at source index register + 2 to AX which was set to the current segment register

	mov dx, #_interrupt_21_service_routine		; get the address of the service interrupt routine
	mov [si], dx								; set up our vector to use the address

	pop ds				; restore data segment register state from stack
	ret


; This is called when an interrupt 21 occurs
; void handle_interrupt_21(int ax, int bx, int cx, int dx)
_interrupt_21_service_routine:
	push dx							; save state on stack for later use
	push cx							;   ...
	push bx							;   ...
	push ax							;   ...

	call _handle_interrupt_21		; call interrupt handler with arguments on stack

	pop ax							; restore state from stack
	pop bx							;   ...
	pop cx							;   ...
	pop dx							;   ...

	iret


; This is used for debugging only, it prints out the contents of i in hexadecimal
; void print_int_hex(int i)
_print_int_hex:
	push bp					; save base pointer register state on stack

	mov bp, sp				; set base pointer register to the stack pointer register for the next step
	mov ax, [bp + 4]		; set AX to the i argument
	call _print_ax_hex		; print AX in hex

	pop bp					; restore base pointer register state from stack
	ret


; This is used for debugging only, it prints out the contents of ax in hexadecimal
; void print_ax_hex()
_print_ax_hex:
	push bx
	push ax
	push ax
	push ax
	push ax
	mov al, ah
	mov ah, #0xe
	mov bx, #7
	shr al, #4
	and al, #0xf
	cmp al, #0xa
	jb ph1
	add al, #0x7
ph1:
	add al, #0x30
	int 0x10

	pop ax
	mov al, ah
	mov ah, #0xe
	and al, #0xf
	cmp al, #0xa
	jb ph2
	add al, #0x7
ph2:
	add al, #0x30
	int 0x10

	pop ax
	mov ah, #0xe
	shr al, #4
	and al, #0xf
	cmp al, #0xa
	jb ph3
	add al, #0x7
ph3:
	add al, #0x30
	int 0x10

	pop ax
	mov ah, #0xe
	and al, #0xf
	cmp al, #0xa
	jb ph4
	add al, #0x7
ph4:
	add al, #0x30
	int 0x10

	pop ax
	pop bx
	ret
