; interrupt.asm
;
; Michael Black, 2007
; Brandon Staab, 2019


; interrupt.asm contains assembly functions that you can use in the shell or kernel
	.global _interrupt


; int interrupt(int number, int ax, int bx, int cx, int dx)
_interrupt:
	push bp				; save base pointer state
	mov bp, sp			; change base pointer to get arguments off the stack

	mov ax, [bp+4]		; get the interrupt number in AL
	push ds				; use self-modifying code to call the right interrupt
	mov bx, cs			; set register BX to the code segment register
	mov ds, bx			; set data segment register to register BX which was set to the code segment register
	mov si, #intr		; set source index register to #intr label

	mov [si+1], al		; change the 00 below to the contents of AL
	pop ds				; pop to the data segment register

	mov ax, [bp+6]		; get the other parameters AX, BX, CX, and DX
	mov bx, [bp+8]		;   ...
	mov cx, [bp+10]		;   ...
	mov dx, [bp+12]		;   ...
intr:
	int #0x00			; call the interrupt (00 will be changed above)
	mov ah, #0			; we only want AL returned
	pop bp				; pop to the base pointer register
	ret
