// clang-format off

#ifndef syscall_blackdos
#define syscall_blackdos


int interrupt(int number, int ax, int bx, int cx, int dx);

#define PRINTC(c)        interrupt(0x21, 0x40, c, 0, 0);
#define PRINTS(c_str)        interrupt(0x21, 0x00, c_str, 0, 0);
#define PRINTN(i)            interrupt(0x21, 0x0D, i, 0, 0);
#define LPRINTS(c_str)       interrupt(0x21, 0x00, c_str, 1, 0);
#define ENDL                 interrupt(0x21, 0x42, 0, 0, 0);
#define LPRINTN(i)           interrupt(0x21, 0x0D, i, 1, 0);
#define SCANS(c_str, size)   interrupt(0x21, 0x01, c_str, size, 0);
#define SCANN(i_ptr)         interrupt(0x21, 0x0E, &i_ptr, 0, 0);
#define END                  interrupt(0x21, 0x05, 0, 0, 0);
#define PAUSE                interrupt(0x21, 0x49, 0, 0, 0);
#define ANY_KEY              PRINTS("Press any key to continue.\r\n\0"); PAUSE;
#define BUSY_STOP            ANY_KEY; END;


#if 0
	#define DEBUG_OUT 0
	#define LOG(msg) interrupt(0x21, 0x00, #msg "\r\n\0", DEBUG_OUT, 0)
	#define LOG_STR(c_str)                          \
		interrupt(0x21, 0x00, c_str, DEBUG_OUT, 0); \
		interrupt(0x21, 0x42, DEBUG_OUT, 0, 0);
	#define LOG_INT(i)                          \
		interrupt(0x21, 0x0D, i, DEBUG_OUT, 0); \
		interrupt(0x21, 0x42, DEBUG_OUT, 0, 0);
#endif

#endif /* end of include guard: syscall_bdos */
