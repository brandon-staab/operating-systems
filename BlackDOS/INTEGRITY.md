ACADEMIC INTEGRITY PLEDGE
=========================

* I have not used source code obtained from another student nor any other unauthorized source, either modified or unmodified.

* All source code and documentation used in my program is either my original work or was derived by me from the source code published in the textbook for this course or presented in class.

* I have not discussed coding details about this project with anyone other than my instructor.  I understand that I may discuss the concepts of this program with other students and that another student may help me debug my program so long as neither of us writes anything during the discussion or modifies any computer file during the discussion.

* I have violated neither the spirit nor letter of these restrictions.

-------------------------------

Signed: *Brandon Staab*

Date: *02/01/2019*

3460:4/526 BlackDOS2020 kernel, Version 1.01, Spring 2018.
