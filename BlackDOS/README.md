**[BlackDOS](https://gitlab.com/brandon-staab/operating-systems)**
==================================================================
_By: **Brandon Staab**_

What is BlackDOS?
-----------------
BlackDOS is an operating system similar to CP/M, designed to fit on a `3.5 inch` floppy disk and bootable on any x86 PC.  This version of BlackDOS is based on material by:
* Dr. Michael Black
* Dr. Grant Braught
* Dr. Tim O'Neil

About
-----
See the [ABOUT.md](./ABOUT.md) for implementation details.

Changelog
---------
See the [CHANGELOG.md](./CHANGELOG.md) for lab report and changes.

Using
-----
See the [USING.md](./USING.md) instructions on how to build and run.
