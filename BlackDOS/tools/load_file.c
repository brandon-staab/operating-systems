/**
 * loadFile.c
 *
 * Michael Black, 2007
 * Tim O'Neil, 2018
 * Brandon Staab, 2019
 *
 * Loads a file into the file system
 * This should be compiled with gcc and run outside of the OS
 */

#include <stdio.h>
#include <stdlib.h>

#define MAP 256
#define DIR 257


int
main(int argc, char* argv[]) {
	int i;

	if (argc < 2) {
		printf("Specify file to load\n");
		return EXIT_FAILURE;
	}

	if (argc < 3) {
		printf("Specify file name to save \"%s\" as\n", argv[1]);
		return EXIT_FAILURE;
	}

	// open the source file
	FILE* load_file;
	load_file = fopen(argv[1], "r");
	if (load_file == NULL) {
		printf("File not found\n");
		return EXIT_FAILURE;
	}

	// open the floppy image
	FILE* floppy;
	floppy = fopen("floppya.img", "r+");
	if (floppy == NULL) {
		printf("floppya.img not found\n");
		return EXIT_FAILURE;
	}

	// load the disk map
	char map[512];
	fseek(floppy, 512 * MAP, SEEK_SET);
	for (i = 0; i < 512; i++) map[i] = fgetc(floppy);

	// load the directory
	char dir[512];
	fseek(floppy, 512 * DIR, SEEK_SET);
	for (i = 0; i < 512; i++) dir[i] = fgetc(floppy);

	// find a free entry in the directory
	for (i = 0; i < 512; i += 0x20)
		if (dir[i] == 0) break;
	if (i == 512) {
		printf("Not enough room in directory\n");
		return EXIT_FAILURE;
	}
	int dir_idx = i;

	// fill the name field with 00s first
	for (i = 0; i < 8; i++) dir[dir_idx + i] = 0x00;
	// copy the name over
	for (i = 0; i < 8; i++) {
		if (argv[2][i] == '\0') break;
		dir[dir_idx + i] = argv[2][i];
	}

	dir_idx = dir_idx + 8;

	// find free sectors and add them to the file
	int sect_count = 0;
	while (!feof(load_file)) {
		if (sect_count == 24) {
			printf("Not enough space in directory entry for file\n");
			return EXIT_FAILURE;
		}

		// find a free map entry
		for (i = 0; i < 256; i++)
			if (map[i] == 0x00) break;
		if (i == 0xFF) {
			printf("Not enough room for file\n");
			return EXIT_FAILURE;
		}

		// mark the map entry as taken
		map[i] = 0xFF;

		// mark the sector in the directory entry
		dir[dir_idx] = i;
		dir_idx++;
		sect_count++;

		// move to the sector and write to it
		fseek(floppy, i * 512, SEEK_SET);
		for (i = 0; i < 512; i++) {
			if (feof(load_file)) {
				fputc(0x0, floppy);
				break;
			} else {
				char c = fgetc(load_file);
				fputc(c, floppy);
			}
		}
	}

	// write the map and directory back to the floppy image
	fseek(floppy, 512 * MAP, SEEK_SET);
	for (i = 0; i < 512; i++) fputc(map[i], floppy);

	fseek(floppy, 512 * DIR, SEEK_SET);
	for (i = 0; i < 512; i++) fputc(dir[i], floppy);

	fclose(floppy);
	fclose(load_file);
}
