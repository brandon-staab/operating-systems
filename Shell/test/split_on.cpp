#include "split_on.hpp"

#include <cassert>


int
main() {
	{  // split on beginning
		auto [first, second] = split_on("abc", 0);
		assert(first == "");
		assert(second == "bc");
	}
	{  // split on first
		auto [first, second] = split_on("abc", 1);
		assert(first == "a");
		assert(second == "c");
	}
	{  // split on second
		auto [first, second] = split_on("abc", 2);
		assert(first == "ab");
		assert(second == "");
	}
	{  // split past end
		auto [first, second] = split_on("abc", 3);
		assert(first == "abc");
		assert(second == "");
	}
	{  // split on infinite
		auto [first, second] = split_on("abc", std::string::npos);
		assert(first == "abc");
		assert(second == "");
	}
	{  // split empty beginning
		auto [first, second] = split_on("", 0);
		assert(first == "");
		assert(second == "");
	}
	{  // split empty end
		auto [first, second] = split_on("", std::string::npos);
		assert(first == "");
		assert(second == "");
	}
	{  // split one beginning/end
		auto [first, second] = split_on("a", 0);
		assert(first == "");
		assert(second == "");
	}
	{  // split one past end
		auto [first, second] = split_on("a", 1);
		assert(first == "a");
		assert(second == "");
	}
}
