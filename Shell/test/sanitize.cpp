#include "sanitize.hpp"

#include <cassert>


int
main() {
	{  // isalnum
		assert("0" == sanitize("0"));
	}
	{  // ispunct
		assert("!" == sanitize("!"));
	}
	{  // isspace
		assert("" == sanitize(" "));
	}
	{  // empty
		assert("" == sanitize(""));
	}
	{  // Ctrl + Z
		assert("" == sanitize("\x1A"));
	}
	{  // convert all whitespace to spaces
		assert("a a" == sanitize("a\ta"));
	}
	{  // multiple valid
		assert("abc 123 !@#" == sanitize("abc 123\t!@#"));
	}
	{  // multiple invalid
		assert("" == sanitize("\x1A\x1B"));
	}
	{  // mixed
		std::string str =
			"\x1A"
			"A"
			"\x1A"
			"1 "
			"\x1A"
			"$";
		assert("A1 $" == sanitize(str));
	}
	{  // duplicate space
		assert("0 0" == sanitize("0   0"));
	}
	{  // leading space
		assert("0" == sanitize(" 0"));
	}
	{  // duplicate leading space
		assert("0" == sanitize("   0"));
	}
	{  // trailing space
		assert("0" == sanitize("0 "));
	}
	{  // duplicate trailing space
		assert("0" == sanitize("0   "));
	}
	{  // only mixed spaces
		assert("" == sanitize(" \t "));
	}
	{  // mixed spaces at start
		assert("0" == sanitize(" \t 0"));
	}
	{  // mixed spaces at end
		assert("0" == sanitize("0 \t "));
	}
	{  // mixed spaces in middle
		assert("0 0" == sanitize("0 \t \t 0"));
	}
	{  // mixed spaces everywhere
		assert("0 0" == sanitize(" \t 0  \t \t\t 0  \t "));
	}
}
