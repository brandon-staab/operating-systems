#include "get_cmd_and_args.hpp"

#include <cassert>


int
main() {
	{  // empty
		std::string const line = "";
		auto [cmd, args] = get_cmd_and_args(line);

		assert(cmd == "");
		assert(args == "");
	}
	{  // just cmd
		std::string const line = "abc";
		auto [cmd, args] = get_cmd_and_args(line);

		assert(cmd == "abc");
		assert(args == "");
	}
	{  // just cmd with spacings
		std::string const line = " \t abc  \t";
		auto [cmd, args] = get_cmd_and_args(line);

		assert(cmd == "abc");
		assert(args == "");
	}
	{  // just single letter
		std::string const line = "a";
		auto [cmd, args] = get_cmd_and_args(line);

		assert(cmd == "a");
		assert(args == "");
	}
	{  // cmd and arg
		std::string const line = "a b";
		auto [cmd, args] = get_cmd_and_args(line);

		assert(cmd == "a");
		assert(args == "b");
	}
	{  // cmd and args
		std::string const line = "a bb ccc d";
		auto [cmd, args] = get_cmd_and_args(line);

		assert(cmd == "a");
		assert(args == "bb ccc d");
	}
	{  // cmd and args with spacing
		std::string const line = "\ta\tb    c dd     e  ";
		auto [cmd, args] = get_cmd_and_args(line);

		assert(cmd == "a");
		assert(args == "b c dd e");
	}
}
