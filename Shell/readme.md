Readme
======

Running
-----
```shell
cd dev
./run.sh
```

Using
-----
After running the shell, execute the `H` command for a help menu.

Help Menu
---------
#### C `src` `dest`
 * Copies a file from dest to src.

#### D `file`
 * Deletes the named file.

#### E `comment`
 * Displays a comment on the screen.

#### H
 * Displays help.

#### L
 * Lists the contents of the current directory.

#### M `file`
 * Opens the file in the text editor.

#### P `file`
 * Prints the contents of the named file.

#### Q
 * Quits the shell

#### S
 * Launches firefox for surfing the web.

#### W
 * Wipes the screen clear.

#### X `program`
 * Executes the named program.
