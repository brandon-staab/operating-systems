#!/bin/sh

reset && (
	cd ../build &&

	make --jobs=4 &&
	make --jobs=4 test &&

	echo "Running..." &&
	./src/shell &&

	echo "Complete"
)
