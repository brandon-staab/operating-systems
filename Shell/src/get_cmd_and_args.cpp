#include "get_cmd_and_args.hpp"

#include "sanitize.hpp"
#include "split_on.hpp"


std::pair<std::string, std::string>
get_cmd_and_args(std::string line) {
	line = sanitize(line);

	return split_on(line, line.find(' '));
}
