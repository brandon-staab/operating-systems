#pragma once

#include <string>
#include <tuple>

std::pair<std::string, std::string> get_cmd_and_args(std::string const line);
