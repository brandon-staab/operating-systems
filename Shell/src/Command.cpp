#include "Command.hpp"

#include "split_on.hpp"

#include <sys/wait.h>
#include <unistd.h>

#include <algorithm>
#include <cstring>
#include <iostream>


Command::Command(std::string const cmd) : m_argv(new char*[count_if(cmd.begin(), cmd.end(), isspace) + 2]), m_data(new char[cmd.length() + 1]) {
	int cmd_idx = 0;
	char* cursor = m_data;
	m_argv[cmd_idx++] = cursor;

	std::strcpy(m_data, cmd.data());
	m_data[cmd.length()] = '\0';

	// add args
	for (auto c : cmd) {
		if (isspace(c)) {
			*cursor++ = '\0';
			m_argv[cmd_idx++] = cursor;
			continue;
		}

		*cursor++ = c;
	}

	*cursor = '\0';
	m_argv[cmd_idx] = nullptr;
}


Command::~Command() {
	delete[] m_argv;
	delete[] m_data;
}


char const*
Command::get_file() const {
	return *m_argv;
}


char* const*
Command::get_argv() const {
	return m_argv;
}


int
Command::exec() const {
	pid_t pid;
	int status;

	pid = fork();
	if (pid < 0) {
		std::cerr << "Could not fork process.\n";
		exit(1);
	} else if (pid == 0) {
		execvp(get_file(), get_argv());

		// falls through if execvp fails
		std::cerr << "Execution failed.\n";
		exit(1);
	} else {
		// clang-format off
		while (wait(&status) != pid);
		// clang-format on
	}

	return status;
}
