#include "cmd/cmd_fwd.hpp"
#include "get_cmd_and_args.hpp"

#include <functional>
#include <iostream>
#include <limits>
#include <unordered_map>


auto constexpr default_prompt = "linux (bls114)|> ";


int
main() {
	std::string line;
	bool is_running = true;

	// command mapping
	std::unordered_map<std::string, std::function<int(std::string)>> mapping = {
		{"C", [](std::string args) { return linux_cmd("cp " + args); }},
		{"D", [](std::string args) { return linux_cmd("rm " + args); }},
		{"E",
		 [](std::string msg) {
			 std::cout << msg << '\n';
			 return 0;
		 }},
		{"H", help},
		{"L", list},
		{"M", [](std::string args) { return linux_cmd("nano " + args); }},
		{"P", [](std::string args) { return linux_cmd("more " + args); }},
		{"Q",
		 [&is_running](std::string) {
			 is_running = false;
			 return 0;
		 }},
		{"S", [](std::string args) { return linux_cmd("firefox " + args); }},
		{"W", [](std::string args) { return linux_cmd("clear " + args); }},
		{"X", run},
	};

	// command loop
	while (is_running) {
		std::cout << default_prompt << std::flush;
		std::getline(std::cin, line);

		// shutdown
		if (std::cin.eof()) break;

		// bad input
		if (!std::cin.good()) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max());
			continue;
		}

		// no input
		if (line.empty()) continue;

		// handle commands
		auto [cmd, args] = get_cmd_and_args(line);
		if (mapping.count(cmd)) {
			if (auto status = mapping[cmd](args)) {
				std::cerr << "Exited with status: " << status << "\n";
			}
		} else {
			std::cout << cmd << ": command not found\n";
		}
	}

	std::cout << "\n\nShell terminated successfully.\n";
}
