#pragma once

#include <memory>
#include <string>
#include <vector>


class Command {
  public:
	Command(std::string const cmd);
	~Command();

	char const* get_file() const;
	char* const* get_argv() const;

	int exec() const;

  private:
	char** const m_argv;
	char* const m_data;
};
