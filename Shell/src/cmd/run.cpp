#include "run.hpp"

#include "get_cmd_and_args.hpp"
#include "linux_cmd.hpp"


int
run(std::string const cmd) {
	if (cmd.empty()) {
		std::cerr << "Please enter a program name to be execute.\n";
		return 0;
	}

	return linux_cmd(cmd);
}
