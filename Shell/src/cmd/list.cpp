#include "list.hpp"

#include "linux_cmd.hpp"

#include <iostream>


int
list(std::string const) {
	std::cout << '\n';
	if (auto status = linux_cmd("pwd -P")) return status;
	std::cout << '\n';
	return linux_cmd("ls -l");
}
