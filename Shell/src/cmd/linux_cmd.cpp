#include "linux_cmd.hpp"

#include "Command.hpp"
#include "sanitize.hpp"

#include <iostream>


int
linux_cmd(std::string const cmd) {
	if (cmd.empty()) {
		std::cerr << "Command cannot be empty.\n";
		return 0;
	}

	return Command(sanitize(cmd)).exec();
}
