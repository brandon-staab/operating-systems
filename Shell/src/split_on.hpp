#pragma once

#include <string>
#include <tuple>

std::pair<std::string, std::string> split_on(std::string const str, std::size_t idx);
