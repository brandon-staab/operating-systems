#include "split_on.hpp"

#include <iostream>
std::pair<std::string, std::string>
split_on(std::string const str, std::size_t idx) {
	if (idx > str.length()) idx = str.length();
	auto str2_beg = (idx == str.length()) ? idx : idx + 1;

	return {str.substr(0, idx), str.substr(str2_beg, str.length())};
}
