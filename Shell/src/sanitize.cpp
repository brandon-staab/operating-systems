#include "sanitize.hpp"

#include <cctype>


namespace {
	inline bool
	is_valid(int c) {
		return isalnum(c) || ispunct(c) || isspace(c);
	}
}  // namespace


std::string
sanitize(std::string const str) {
	std::string output;
	output.reserve(str.length());

	bool last_was_space = true;
	for (auto const c : str) {
		// skip duplicate spaces
		if (last_was_space && isspace(c)) continue;

		// ignore invalid
		if (is_valid(c)) {
			// write if space and update flag
			if ((last_was_space = isspace(c))) {
				output += ' ';
				continue;
			}

			output += c;
		}
	}

	// remove trailing space
	if (last_was_space && !output.empty()) output.pop_back();

	return output;
}
